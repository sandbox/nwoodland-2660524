This simple module highlights newly created blocks in the Blocks administration 
interface. This can be useful if you have regions populated with numerous 
blocks, as it will allow you to find and reorder new blocks instantly. New 
blocks are only highlighted once per theme with red text reading "New", and a 
temporary pink background that fades once the new block has scrolled into view.

The module is lightweight and makes no changes to existing database tables or 
templates, and highlights blocks of any origin (basic user-created blocks, view
blocks, menu blocks, blocks added by modules, etc.).
