/**
 * @file
 * Adds temporary highlighting of table rows containing new blocks.
 */

jQuery(document).ready(function ($) {
  'use strict';
  $('form#block-admin-display-form tr.new-block').addClass('new-block-unseen new-block-transition');
  $('form#block-admin-display-form tr.new-block td.block').wrapInner('<div class="new-block-wrapper"></div>');
  $('form#block-admin-display-form tr.new-block td.block div.new-block-wrapper').append('<div class="new-block-indicator">' + Drupal.t('New') + '</div>');
  function newBlockInView(el) {
    var $el = $(el);
    var $window = $(window);
    var docViewTop = $window.scrollTop();
    var docViewBottom = docViewTop + $window.height();
    var elTop = $el.offset().top;
    var elBottom = elTop + $el.height();
    return ((elBottom <= docViewBottom) && (elTop >= docViewTop));
  }
  if ($('tr.new-block').length) {
    $(window).scroll(function () {
      if (newBlockInView('tr.new-block')) {
        setTimeout(function () {
          $('form#block-admin-display-form tr.new-block').removeClass('new-block-unseen');
        }, 1000);
        setTimeout(function () {
          $('form#block-admin-display-form tr.new-block').removeClass('new-block-transition');
        }, 2000);
      }
    });
  }
});
